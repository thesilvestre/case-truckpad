# Ambiente de desenvolvimento

## Depedências
python3, SQLite3

## Rodando com Docker
Dentro do diretório do projeto, rode:

```bash
docker build -t truckpad .
```

```bash
docker run -it -p 8080:8080 -v $PWD:/usr/src/app truckpad
```

## Rodando sem Docker
Dentro do diretório do projeto, rode:

```bash
python3 app.py
```

## Acessando a API
No browser, entre com o endereço http://localhost:8080 e encontrará uma lista com as entidades e seus respectivos endpoints e parametros.

Para acessar um endpoint pode ser usado o app Postman ou via console usando o Curl.
Ex:
```bash
curl -i localhost:8080/truck_types ; echo
curl -i localhost:8080/trucks ; echo
curl -i localhost:8080/drivers ; echo
curl -i localhost:8080/truck_travels ; echo
```

# Considerações sobre o Projeto

## Framework
Foi escolhido o CherryPy pela leveza e simplicidade, a ideia foi ter menos código desnecessário.

## Banco de dados
Para fins de agilidade e gestão de menos dependências, foi escolhido o SQLite como banco de dados. Em produção, minha escolha seria Postgres ou Mysql Aurora.

## Possíveis melhorias
* Usar uma lib de ORM para não ter SQLs no próprio código
* Criar arquitetura MVC
* Refactoring para deixar o código mais DRY (Possuí muita repetição)
* Implementar testes
* Implementar validações
