import sqlite3 as DB
import time
import cherrypy

DB_NAME = 'truckpad.db'

class TruckTypes:
  exposed = True

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT * FROM truck_types')
      results = query.fetchall()

      if results:
        cherrypy.response.status = 200
        return [dict(id=r[0], name=r[1], status=r[2]) for r in results]
      
      return []
  
  def POST(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('INSERT INTO truck_types (name, status, created_at, updated_at) \
                            VALUES (?, ?, ?, ?)', 
                            [params['name'], params['status'], time.time(), time.time()])
    
    if result:
      cherrypy.response.status = 201
      return dict(success=True)

    return dict(success=False)

  def PATCH(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('UPDATE truck_types SET name=?, status=?, updated_at=? WHERE id=?', 
                            [params['name'], params['status'], time.time(), params['id']])
    
    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

  def DELETE(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('DELETE FROM truck_types WHERE id=?', [params['id']])

    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

class Trucks:
  exposed = True

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT * FROM trucks')
      results = query.fetchall()

    if results:
      cherrypy.response.status = 200
      return [dict(id=r[0], driver_id=r[3], truck_type_id=r[4]) for r in results]
    
    return []

  def POST(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('INSERT INTO trucks (driver_id, truck_type_id, created_at, updated_at) \
                            VALUES (?, ?, ?, ?)', 
                            [params['driver_id'], params['truck_type_id'], time.time(), time.time()])
    
    if result:
      cherrypy.response.status = 201
      return dict(success=True)

    return dict(success=False)

  def PATCH(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('UPDATE trucks SET driver_id=?, truck_type_id=?, updated_at=? WHERE id=?', 
                            [params['driver_id'], params['truck_type_id'], time.time(), params['id']])
    
    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

  def DELETE(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('DELETE FROM trucks WHERE id=?', [params['id']])

    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

class DriversWithOwnVehicle:
  exposed = True

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT COUNT(1) FROM drivers WHERE own_vehicle=?', [1])
      result = query.fetchone()
      
    if result:
      cherrypy.response.status = 200
      return dict(drivers_with_own_vehicle=result[0])

    return dict(message='Nenhum resultado')

class Drivers:
  exposed = True
  # /drivers/drivers_with_own_vehicle
  drivers_with_own_vehicle = DriversWithOwnVehicle()

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT * FROM drivers')
      results = query.fetchall()

    if results:
      cherrypy.response.status = 200
      return [dict(id=r[0], 
                   name=r[1], 
                   age=r[2], 
                   gender=r[3], 
                   own_vehicle=r[4], 
                   cnh_type=r[5], 
                   status=r[6]) for r in results]
    
    return []

  def POST(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('INSERT INTO drivers \
                            (name, age, gender, own_vehicle, cnh_type, status, \
                             created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', 
                            [params['name'],
                             params['age'],
                             params['gender'],
                             params['own_vehicle'],
                             params['cnh_type'],
                             params['status'], 
                             time.time(), 
                             time.time()])
    
    if result:
      cherrypy.response.status = 201
      return dict(success=True)

    return dict(success=False)

  def PATCH(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('UPDATE drivers SET name=?, \
                                               age=?, \
                                               gender=?, \
                                               own_vehicle=?, \
                                               cnh_type=?, \
                                               status=?, \
                                               updated_at=? WHERE id=?', 
                                              [params['name'], 
                                               params['age'],
                                               params['gender'],
                                               params['own_vehicle'],
                                               params['cnh_type'],
                                               params['status'],
                                               time.time(), 
                                               params['id']])
    
    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

  def DELETE(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('DELETE FROM drivers WHERE id=?', [params['id']])

    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

class TruckTravelListOriginsAndDestinations:
  exposed = True

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT origin_latitude, origin_longitude FROM truck_travels \
                           GROUP BY origin_latitude, origin_longitude')
      results_origins = query.fetchall()

      query = con.execute('SELECT destination_latitude, destination_longitude FROM truck_travels \
                           GROUP BY destination_latitude, destination_longitude')
      results_destinations = query.fetchall()
      
    if results_origins and results_destinations:
      cherrypy.response.status = 200
      return {'origins':      [dict(origin_latitude=r[0], origin_longitude=r[1]) 
                               for r in results_origins],
              'destinations': [dict(destinations_latitude=r[0], destinations_longitude=r[1]) 
                               for r in results_destinations]}

    return {'origins': [], 'destinations': []}

class TruckTravelsWithoutLoadDeparture:
  exposed = True

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT COUNT(1) FROM truck_travels WHERE load_departure=?', [0])
      result = query.fetchone()
      
    if result:
      cherrypy.response.status = 200
      return dict(drivers_without_load_departure=result[0])

    return dict(message='Nenhum resultado')

class TruckTravelsLoadedArrival:
  exposed = True

  def POST(self):
    params = cherrypy.request.json
    
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT COUNT(1) FROM truck_travels WHERE load_arrival=? AND \
                           arrival_date>=? AND arrival_date<=?', 
                           [1, params['arrival_date_period_start'], params['arrival_date_period_end']])
      result = query.fetchone()
      
    if result:
      cherrypy.response.status = 200
      return dict(loaded_trucks=result[0])

    return dict(message='Nenhum resultado')

class TruckTravels:
  exposed = True
  # truck_travels/list_origins_and_destinations
  list_origins_and_destinations = TruckTravelListOriginsAndDestinations()
  # /truck_travels/drivers_without_load_departure
  drivers_without_load_departure = TruckTravelsWithoutLoadDeparture()
  #truck_travels/loaded_arrival
  loaded_arrival = TruckTravelsLoadedArrival()

  def GET(self):
    with DB.connect(DB_NAME) as con:
      query = con.execute('SELECT * FROM truck_travels')
      results = query.fetchall()

    if results:
      cherrypy.response.status = 200
      return [dict(id=r[0], 
                   truck_id=r[1], 
                   driver_id=r[2], 
                   load_arrival=r[3], 
                   load_departure=r[4], 
                   arrival_date=r[5], 
                   departure_date=r[6], 
                   origin_latitude=r[7],
                   origin_longitude=r[8],
                   destination_latitude=r[9],
                   destination_longitude=r[10],
                   created_at=r[11]) for r in results]

    return []

  def POST(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('INSERT INTO truck_travels \
                            (truck_id, driver_id, load_arrival, load_departure, arrival_date, \
                             departure_date, origin_latitude, origin_longitude, \
                             destination_latitude, destination_longitude, created_at, updated_at) \
                             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                            [params['truck_id'],
                             params['driver_id'],
                             params['load_arrival'],
                             params['load_departure'],
                             params['arrival_date'],
                             params['departure_date'], 
                             params['origin_latitude'],
                             params['origin_longitude'],
                             params['destination_latitude'],
                             params['destination_longitude'], 
                             time.time(), 
                             time.time()])

    if result:
      cherrypy.response.status = 201
      return dict(success=True)

    return dict(success=False)

  def PATCH(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('UPDATE truck_travels SET truck_id=?, \
                                                     driver_id=?, \
                                                     load_arrival=?, \
                                                     load_departure=?, \
                                                     arrival_date=?, \
                                                     departure_date=?, \
                                                     origin_latitude=?, \
                                                     origin_longitude=?, \
                                                     destination_latitude=?, \
                                                     destination_longitude=?, \
                                                     updated_at=? WHERE id=?', 
                                                    [params['truck_id'], 
                                                     params['driver_id'],
                                                     params['load_arrival'],
                                                     params['load_departure'],
                                                     params['arrival_date'],
                                                     params['departure_date'],
                                                     params['origin_latitude'],
                                                     params['origin_longitude'],
                                                     params['destination_latitude'],
                                                     params['destination_longitude'],
                                                     time.time(), 
                                                     params['id']])
    
    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

  def DELETE(self):
    params = cherrypy.request.json

    with DB.connect(DB_NAME) as con:
      result = con.execute('DELETE FROM truck_travels WHERE id=?', [params['id']])

    if result:
      cherrypy.response.status = 202
      return dict(success=True)

    return dict(success=False)

class API(object):
  _cp_config  = {'tools.json_out.on': True,
                 'tools.json_in.on': True}
  exposed = True
  truck_types = TruckTypes()
  trucks = Trucks()
  drivers = Drivers()
  truck_travels = TruckTravels()

  def GET(self):
    return {
      'TruckTypes': {
        'GET': '/truck_types',
        'POST': '/truck_types',
        'PATCH': '/truck_types',
        'DELETE': '/truck_types',
        'params': 'id, name, status'
      },
      'Trucks': {
        'GET': '/trucks',
        'POST': '/trucks',
        'PATCH': '/trucks',
        'DELETE': '/trucks',
        'params': 'id, driver_id, truck_type_id'
      },
      'Drivers': {
        'GET': '/drivers',
        'POST': '/drivers',
        'PATCH': '/drivers',
        'DELETE': '/drivers',
        'GET .': '/drivers/drivers_with_own_vehicle',
        'params': 'id, name, age, own_vehicle, cnh_type, status'
      },
      'TruckTravels': {
        'GET': '/truck_travels',
        'POST': '/truck_travels',
        'PATCH': '/truck_travels',
        'DELETE': '/truck_travels',
        'GET .': '/truck_travels/list_origins_and_destinations',
        'GET ..': '/truck_travels/drivers_without_load_departure',
        'GET ...': '/truck_travels/loaded_arrival',
        'params': 'id, driver_id, load_arrival, load_departure, arrival_date, departure_date, \
                   origin_latitude, origin_longitude, destination_latitude, destination_longitude'
      }
    }

def setup_database():
  with DB.connect(DB_NAME) as con:
    con.execute('CREATE TABLE IF NOT EXISTS truck_types \
                (id integer PRIMARY KEY AUTOINCREMENT, \
                 name varchar(255), \
                 status boolean, \
                 created_at datetime, \
                 updated_at datetime)')

    con.execute('CREATE TABLE IF NOT EXISTS trucks \
                (id integer PRIMARY KEY AUTOINCREMENT, \
                 created_at datetime, \
                 updated_at datetime, \
                 driver_id integer, \
                 truck_type_id integer, \
                 CONSTRAINT fk_truck_types \
                 FOREIGN KEY (truck_type_id) \
                 REFERENCES truck_types(truck_type_id), \
                 CONSTRAINT fk_drivers \
                 FOREIGN KEY (driver_id) \
                 REFERENCES drivers(driver_id))')

    con.execute('CREATE TABLE IF NOT EXISTS drivers \
                (id integer PRIMARY KEY AUTOINCREMENT, \
                 name varchar(256), \
                 age integer, \
                 gender varchar(10), \
                 own_vehicle integer, \
                 cnh_type varchar(256), \
                 status integer, \
                 created_at datetime, \
                 updated_at datetime)')

    con.execute('CREATE TABLE IF NOT EXISTS truck_travels \
                (id integer PRIMARY KEY AUTOINCREMENT, \
                 truck_id integer, \
                 driver_id integer, \
                 load_arrival integer, \
                 load_departure integer, \
                 arrival_date datetime, \
                 departure_date datetime, \
                 origin_latitude varchar(256), \
                 origin_longitude varchar(256), \
                 destination_latitude varchar(256), \
                 destination_longitude varchar(256), \
                 created_at datetime, \
                 updated_at datetime, \
                 CONSTRAINT fk_trucks \
                 FOREIGN KEY (truck_id) \
                 REFERENCES trucks(truck_id), \
                 CONSTRAINT fk_drivers \
                 FOREIGN KEY (driver_id) \
                 REFERENCES drivers(driver_id))')

def cleanup_database():
  with DB.connect(DB_NAME) as con:
    con.execute('DROP TABLE trucks')

if __name__ == '__main__':
  cherrypy.config.update({'server.socket_host': '0.0.0.0'})

  config = {
    '/': {
      'request.methods_with_bodies': ('POST', 'PUT', 'PATCH', 'DELETE'),
      'request.dispatch': cherrypy.dispatch.MethodDispatcher()
    }
  }

cherrypy.engine.subscribe('start', setup_database)
#cherrypy.engine.subscribe('stop', cleanup_database)

cherrypy.quickstart(API(), config=config)

cherrypy.engine.start()
cherrypy.engine.block()